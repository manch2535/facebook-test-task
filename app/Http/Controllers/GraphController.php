<?php

namespace App\Http\Controllers;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GraphController extends Controller
{
    private $api;


    /**
     * GraphController constructor.
     *
     * @param Facebook $fb
     */
    public function __construct(Facebook $fb)
    {
        $this->middleware(function ($request, $next) use ($fb) {
            $fb->setDefaultAccessToken(Auth::user()->token);
            $this->api = $fb;
            return $next($request);
        });
    }


    /**
     * @return \Exception|FacebookSDKException
     */
    public function retrieveUserProfile(){
        try {

            $params = "first_name,last_name,age_range,gender";

            $user = $this->api->get('/me?fields='.$params)->getGraphUser();
            $response = $user;

        } catch (FacebookSDKException $e) {
            $response = $e;
        }

        return $response;
    }


    /**
     * @param Request $request
     *
     * @return \Exception|FacebookSDKException
     */
    public function publishToProfile(Request $request){
        try {
            $response = $this->api->post('/me/feed', [
                'message' => $request->message
            ]);

        } catch (FacebookSDKException $e) {
            $response = $e;
        }

        return $response;
    }


    /**
     * @return \Exception|FacebookSDKException
     */
    public function publishToPage(){

        $page_id = '257545431025617';

        try {
            $post = $this->api->post('/' . $page_id . '/feed', array('message' => 'New post...'), $this->getPageAccessToken($page_id));

            $post = $post->getGraphNode()->asArray();

            $response = $post;

        } catch (FacebookSDKException $e) {
            $response = $e;
        }

        return $response;
    }


    /**
     * @param $page_id
     *
     * @return \Exception|FacebookSDKException|string
     */
    public function getPageAccessToken($page_id){
        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $this->api->get('/me/accounts', Auth::user()->token);
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            $response = 'Graph returned an error: ' . $e->getMessage();
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            $response = 'Facebook SDK returned an error: ' . $e->getMessage();
        }

        try {
            $pages = $response->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['id'] == $page_id) {
                    $response =  $key['access_token'];
                }
            }
        } catch (FacebookSDKException $e) {
            $response  = $e; // handle exception
        }

        return $response;
    }
}
